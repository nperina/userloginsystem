
//read files 

//upload data 
//import axios from 'axios';
const fs = require('fs-extra');
const axios = require('axios');
//readIn('F1-1354.log');
scanFolder(); 

function scanFolder() {

		//read files
		const currentFolder = './';
		fs.readdirSync(currentFolder).forEach(file => {
				if (file.endsWith(".log")){
						readIn(file); 
				}
		});

		//move files 
		var today = new Date();
		const todayDir = './backups/' + today.toISOString().split("T")[0];
		if (!fs.existsSync(todayDir)){
    		fs.mkdirSync(todayDir);
		}

		fs.readdirSync(currentFolder).forEach(file => {
			if (file.endsWith(".log")){
				var newPath = todayDir + "/" + file;
				var oldPath = currentFolder + file; 
				fs.rename(oldPath, newPath, function (err) {
			        if (err) {
			            console.log(err);
			            return;
			        }
        			return;
    			});
			}
			
		});

		//delete backups more than 30 days old 
		var backsupsDir = './backups/';
		fs.readdirSync(currentFolder).forEach(file => {
				var parts = file.split("-");
				var year = parseInt(parts[0]);
				var month = parseInt(parts[1]);
				var day = parseInt(parts[2]);

				var date = new Date();
				date.setMonth(month);
				date.setYear(year);
				date.setDate(day);
				var today = new Date(); 
				var thirtyAgo = new Date().setDate(today.getDate()-30);
				var path = backsupsDir + file
				if (date < thirtyAgo){
					fs.remove(path, err => {
  						if (err) {
  							console.error(err);
  							return; 
  						}

  						console.log('deleted: ' + path);
					})
				}
		});




}



function checkLogoff(logOnEntry, currentLogOffIndex, logOffs){
		if (currentLogOffIndex >= logOffs.length){
			return; 
		}
		if (logOffs[currentLogOffIndex].user != logOnEntry.user){
			return; 
		}
		if (logOffs[currentLogOffIndex].date != logOnEntry.date){
			return;
		}
		return logOffs[currentLogOffIndex].dateTime.getTime() - logOnEntry.dateTime.getTime();

}

function sendToApi(sessions, cb){
		console.log("sending to api....");
		console.log("sessions:");
		console.log(sessions);
		url = 'http://productivity.digitaldividedata.com:8111/api/sessions/pnh';
		axios.post(url, {
    		"sessions": sessions
  		})
  		.then(function (response) {
    		cb(response);
  		})
  		.catch(function (error) {
    		cb(error);
  		});
}



function parseData(logOns, logOffs, locks, users, cb){
	
	var currentLogOff = 0; 
	var currentLockIndex = 0; 
	var currentUnlockIndex = 1; 
	var duration = 0;
	var sessions = []; 

	logOns.forEach(function(entry){

		try {
			duration = checkLogoff(entry, currentLogOff, logOffs);
			if (!(duration)){
				return; 
			}
			else if (duration < 0 ){
				currentLogOff++; 
				duration = checkLogoff(entry, currentLogOff, logOffs);
				if (!(duration)){
					return; 
				}
				else if (duration < 0){
					return; 
				}
			}

			session = {};
			session['user'] = entry.user; 
			session['logOnTime'] = entry.dateTime; 
			session['logOffTime'] = logOffs[currentLogOff].dateTime;
			session['duration'] = duration; 

		
			var lockCount = 0; 
			var totalLockDuration = 0; 

			if( currentLockIndex < locks.length && 
				currentUnlockIndex < locks.length){
				var currentLock = locks[currentLockIndex];
				var currentUnlock = locks[currentUnlockIndex];


				while (currentLock.dateTime.getTime() <= entry.dateTime.getTime()){
					currentLockIndex++;
					currentUnlockIndex++; 
					if(currentLockIndex < locks.length && currentUnlockIndex < locks.length){
						currentLock = locks[currentLockIndex];
						currentUnlock = locks[currentUnlockIndex];
					}
					else{
						break; 
					}
				}

				while(currentLock.dateTime.getTime() >= entry.dateTime.getTime()
					&& currentUnlock.dateTime.getTime() <= logOffs[currentLogOff].dateTime.getTime()
					){
					if (currentLock.eventType == "lock" 
						&& currentUnlock.eventType == "lock"){
						//console.log("Both locks --> incrementing")
						currentLockIndex++; 
						currentUnlockIndex++; 
						
					}
					else if (currentLock.eventType == "lock" && 
						currentUnlock.eventType == "unlock"){
						if(currentUnlock.dateTime.getTime() > 
							currentLock.dateTime.getTime() && currentLock.user == entry.user && currentUnlock.user == entry.user){
							lockCount++; 
							totalLockDuration = totalLockDuration + (currentUnlock.dateTime.getTime() - currentLock.dateTime.getTime()) 
						}
						currentLockIndex = currentLockIndex + 2; 
						currentUnlockIndex = currentUnlockIndex + 2; 
					}
					else if (currentLock.eventType == "unlock" &&
							currentUnlock.eventType == "lock"){
						currentLockIndex++;
						currentUnlockIndex++;
					}
					else {
						currentLockIndex = currentLockIndex + 2; 
						currentUnlockIndex = currentUnlockIndex + 2; 
					}

					if (currentLockIndex >= locks.length || 
						currentUnlockIndex >= locks.length){
						break;
					}
					else{
						currentLock = locks[currentLockIndex];
						currentUnlock = locks[currentUnlockIndex];
					}
				}
			}
		
			session['lockOutCount'] = lockCount; 
			session['totalLockOutDuration'] = totalLockDuration; 
			currentLogOff++;
			sessions.push(session);

		}
		catch(err){
			console.log("ERROR processing " + filename  + ": ");
			console.log(err.message);
		}
	}); 
	cb(sessions);




}


function readIn(filename){

	var lineReader = require('readline').createInterface({
  		input: require('fs').createReadStream(filename)
	});

	var users = []
	var logOns = []
	var logOffs = []
	var stationLocks = []

	lineReader.on('line', function (line) {
		if (line.includes("***")){
			return
		} 
		else{
			lineParts = line.trim().split(/\s+/);
			var offset = (lineParts.length == 8)?(1):(0);
			var user = ""; 
			var date = ""; 
			var time = ""; 
			var dateTime; 
			var type = ""


			if (line.includes("WORK STATION")){
				var offset = (lineParts.length == 8)?(1):(0);
				user = lineParts[3];
				date = lineParts[4 + offset];
				time = lineParts[5 + offset].split(".")[0];
				comp = lineParts[6 + offset];
				if (time.split(":")[0].length == 1){
					time = "0" + time; 
				}
				year = date.split("/")[2];
				month = date.split("/")[0];
				day = date.split('/')[1]; 
				dateString = year + "-" + month + "-" + day + "T" + time + "Z"
				if (lineParts[2].includes("LOCK")){
					type = "lock";
				}
				if (lineParts[2].includes("UNLOCK")){
					type = "unlock";
				}
			}
			else if(line.includes("LOG")){
				var offset = (lineParts.length == 7)?(1):(0);
				user = lineParts[2];
				date = lineParts[3 + offset];
				time = lineParts[4 + offset].split('.')[0];
				year = date.split("/")[2];
				month = date.split("/")[0];
				day = date.split('/')[1]; 
				if (time.split(":")[0].length == 1){
					time = "0" + time; 
				}

				dateString = year + "-" + month + "-" + day + "T" + time + "Z";
				comp = lineParts[5 + offset];
				if (lineParts[1].includes("OFF")){
					type = "logOff"
				}
				if (lineParts[1].includes("ON")){
					type = 'logOn'
				}
			}
			else {
				return;
			}
			if (!(users.includes(user))){
				users.push(user);
			}
			dateTime = new Date(dateString);
			date = new Date(dateString).setHours(0,0,0,0);
			obj = { "eventType": type, 
					 "user": user, 
					 "dateTime": dateTime,
					 "date": date,
					 "comp": comp
					}
			//console.log(obj);
			if (type == "logOn"){
				logOns.push(obj);
			}
			else if (type == 'logOff'){
				logOffs.push(obj);
			}
			else if (type == "lock"){
				stationLocks.push(obj)
			}
			else if (type == "unlock"){
				stationLocks.push(obj)
			}
				
		}

			
	});

	lineReader.on('close', function(){
		parseData(logOns, logOffs, stationLocks,  users, function(sessions){
			if (sessions.length > 0){
				sendToApi(sessions, function(response){
					console.log("response:");
					console.log(response);
					if (response.error){
						console.log("ERROR:");
						console.log(response.error);

					}
					else if (response.result){
						console.log("RESULT:");
						console.log(response.result);
					}
				});
			}
				 
		});
	}); 
	

}




